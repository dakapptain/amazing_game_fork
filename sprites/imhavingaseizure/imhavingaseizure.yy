{
    "id": "617eafce-5b5c-4216-8137-32de52bf7041",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "imhavingaseizure",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "697f86d3-19c9-40b3-b180-3a9776d36e9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "617eafce-5b5c-4216-8137-32de52bf7041",
            "compositeImage": {
                "id": "2be019d8-d5b5-48cd-b154-90076f8390da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "697f86d3-19c9-40b3-b180-3a9776d36e9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26d8bb11-4466-48d1-9b7f-a9d33ff155d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "697f86d3-19c9-40b3-b180-3a9776d36e9b",
                    "LayerId": "f3f4ed74-7634-4eef-a07d-44eff7afcb33"
                }
            ]
        },
        {
            "id": "b48883fe-0142-4be0-823d-6e95167583c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "617eafce-5b5c-4216-8137-32de52bf7041",
            "compositeImage": {
                "id": "a4a6be70-797f-4432-8e17-cecd3c3d7e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48883fe-0142-4be0-823d-6e95167583c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5d75b76-1a56-4f2a-8dad-aa8e34c23bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48883fe-0142-4be0-823d-6e95167583c7",
                    "LayerId": "f3f4ed74-7634-4eef-a07d-44eff7afcb33"
                }
            ]
        },
        {
            "id": "2d19ccb2-4793-4127-b4d4-6ac35bb49049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "617eafce-5b5c-4216-8137-32de52bf7041",
            "compositeImage": {
                "id": "3a6e2c29-5779-4334-8443-068219e90f15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d19ccb2-4793-4127-b4d4-6ac35bb49049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "738b06cc-0899-41a8-bf61-80cddb557351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d19ccb2-4793-4127-b4d4-6ac35bb49049",
                    "LayerId": "f3f4ed74-7634-4eef-a07d-44eff7afcb33"
                }
            ]
        },
        {
            "id": "0d05cf3e-baaf-4ecd-b7a7-6883b516c435",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "617eafce-5b5c-4216-8137-32de52bf7041",
            "compositeImage": {
                "id": "0178bac5-157c-4267-b000-56e7ce851094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d05cf3e-baaf-4ecd-b7a7-6883b516c435",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc165d72-9599-4ca4-b4c7-2b4016a33db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d05cf3e-baaf-4ecd-b7a7-6883b516c435",
                    "LayerId": "f3f4ed74-7634-4eef-a07d-44eff7afcb33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f3f4ed74-7634-4eef-a07d-44eff7afcb33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "617eafce-5b5c-4216-8137-32de52bf7041",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}