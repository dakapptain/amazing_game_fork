{
    "id": "deef2094-e521-4912-bafe-dd4869d3ed30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "CORCLE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "989eb827-ff74-47f3-813b-48f8411e567f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "deef2094-e521-4912-bafe-dd4869d3ed30",
            "compositeImage": {
                "id": "f06d6bdd-1dc3-4095-b20f-3e6fe7d00c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "989eb827-ff74-47f3-813b-48f8411e567f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d90c38a1-33e3-4e77-9b95-dc6697da02df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "989eb827-ff74-47f3-813b-48f8411e567f",
                    "LayerId": "0e1a1290-08c8-4e93-8ca1-20faa05c0b9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0e1a1290-08c8-4e93-8ca1-20faa05c0b9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "deef2094-e521-4912-bafe-dd4869d3ed30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}