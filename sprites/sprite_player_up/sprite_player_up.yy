{
    "id": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2ae4179-d654-4d79-aafe-47887d1ebb30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "compositeImage": {
                "id": "9c2f9457-f5eb-4893-a35d-5ffa0453c0b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ae4179-d654-4d79-aafe-47887d1ebb30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b589f2d7-316d-425a-965a-20dce387fe04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ae4179-d654-4d79-aafe-47887d1ebb30",
                    "LayerId": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a"
                }
            ]
        },
        {
            "id": "244e62b1-a216-4996-9825-92e427278c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "compositeImage": {
                "id": "64247295-ded6-471e-bc8a-c301ebbaec65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244e62b1-a216-4996-9825-92e427278c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6dc74a-de03-4c93-91fe-f11dc0bfd568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244e62b1-a216-4996-9825-92e427278c4e",
                    "LayerId": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a"
                }
            ]
        },
        {
            "id": "ea0523c0-1009-4236-b558-ae5e45977db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "compositeImage": {
                "id": "1baadd8c-81cd-4a76-9d60-c7bb50842c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea0523c0-1009-4236-b558-ae5e45977db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5afd120-9e70-42d0-b7cf-deb3f95c35cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea0523c0-1009-4236-b558-ae5e45977db2",
                    "LayerId": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a"
                }
            ]
        },
        {
            "id": "d8d9c6cb-483d-4ce7-8757-531ced4058a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "compositeImage": {
                "id": "c74e7757-3bc1-4ad3-a9de-8b841f4cf48f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8d9c6cb-483d-4ce7-8757-531ced4058a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09634bb2-f74d-4065-82e4-ad58ed33e67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8d9c6cb-483d-4ce7-8757-531ced4058a9",
                    "LayerId": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a"
                }
            ]
        },
        {
            "id": "260cf896-938b-4d19-9cb3-49d808107f2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "compositeImage": {
                "id": "202b8fea-9fdf-4447-a8e2-b7fbfb56a153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "260cf896-938b-4d19-9cb3-49d808107f2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b122069e-fa82-4247-a2a2-3d12a24016ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "260cf896-938b-4d19-9cb3-49d808107f2a",
                    "LayerId": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a"
                }
            ]
        },
        {
            "id": "f05de5a3-1f7d-4672-9f6a-4452a7bbf224",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "compositeImage": {
                "id": "9a96690e-d102-4c75-a7ba-6dee656186f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f05de5a3-1f7d-4672-9f6a-4452a7bbf224",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d0863bf-c646-4953-b0be-ba5f190c4cda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f05de5a3-1f7d-4672-9f6a-4452a7bbf224",
                    "LayerId": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a"
                }
            ]
        },
        {
            "id": "2a6c76df-e0ee-4412-9162-df4c54d38d16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "compositeImage": {
                "id": "8815265e-7207-479a-ac8d-1dcde22640d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a6c76df-e0ee-4412-9162-df4c54d38d16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "608db3f4-94e6-46c0-b91e-4e2f99b6d19d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a6c76df-e0ee-4412-9162-df4c54d38d16",
                    "LayerId": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a"
                }
            ]
        },
        {
            "id": "204202ac-7f16-4e10-9fb8-3cbff1ee9b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "compositeImage": {
                "id": "65f039da-8915-4bac-8686-f2867fdeae6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "204202ac-7f16-4e10-9fb8-3cbff1ee9b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0779662-dcb1-453a-8339-a33abdcd75f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "204202ac-7f16-4e10-9fb8-3cbff1ee9b8c",
                    "LayerId": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "59ed23f0-91f2-42e3-a7e5-2fdb197e527a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ee8e8a9-1768-4215-b5db-8c13d7278b97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}